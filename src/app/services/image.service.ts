import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

//Url global API
import { GLOBAL } from '../services/global';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ImageService {

  public url: string;
  constructor(private _httpClient: HttpClient) {
    this.url = GLOBAL.url;

  }

  //guardar ruta de imagen en album
  saveImage(image): Observable<any> {
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let params = JSON.stringify(image);
    return this._httpClient.post(this.url + 'image', params, { headers: headers });
  }

  //extraer listado imágenes de un álbum
  getImages(albumId=null):Observable<any> {
    return this._httpClient.get(this.url+'images/'+albumId);
  }

  //extraer imagen por su Id
  getImage(imageId):Observable<any>{
    return this._httpClient.get(this.url+'image/'+imageId);
  }

  //actualizar imagen
  updateImage(imageId,image):Observable<any>{
    let headers=new HttpHeaders({'Content-Type':'application/json'});
    let params=JSON.stringify(image);

    return this._httpClient.put(this.url+'image/'+imageId,params,{headers:headers});
  }

  deleteImage(imageId):Observable<any>{
    return this._httpClient.delete(this.url+'image/'+imageId);
  }

}


