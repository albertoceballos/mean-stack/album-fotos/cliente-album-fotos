import { Component, OnInit } from '@angular/core';

//servicios
import { AlbumService } from '../../services/album.service';
import { ImageService } from '../../services/image.service';

import { Album } from '../../models/Album';

import {GLOBAL} from '../../services/global';


@Component({
  selector: 'app-albums-list',
  templateUrl: './albums-list.component.html',
  styleUrls: ['./albums-list.component.css'],
  providers: [AlbumService],
})
export class AlbumsListComponent implements OnInit {
  public pageTitle: string;
  public albums: Array<Album>;
  public status;
  public loading;
  public deleteStatus: string;
  public message: string;
  public url;
  public imagesArray;
  constructor(private _albumService: AlbumService, private _imageService: ImageService) {
    this.pageTitle = "Lista de álbums de fotos";
    this.loading = true;
    this.url=GLOBAL.url;
  }

  ngOnInit() {
   this.getAllAlbums();
  }
   getAllAlbums(){
   this._albumService.getAlbums().subscribe(
      response => {
        if (response.albums) {
          this.status = 'success';
          this.albums = response.albums;
          this.imagesArray=[];
          this.albums.forEach((element,index)=> {
            this._imageService.getImages(element._id).subscribe(
              resp=>{
                this.imagesArray[element._id]=resp.images;
              },
              err=>{
                console.log(err);
              }
            )
          });
        } else {
          this.status = 'error';
        }
        this.loading = false;
      },
      error => {
        console.log(error);
        this.status = 'error';
      }
    );
  }


  deleteAlbum(idAlbum) {
    this._albumService.deleteAlbum(idAlbum).subscribe(
      response => {
        if (response.album) {
          this.deleteStatus = 'success';
          this.message = response.message;
          //this.getAllAlbums();
          setTimeout(() => {
            this.deleteStatus = null;
          }, 3000);
        } else {
          this.deleteStatus = 'error';
          this.message = 'No se pudo borrar el álbum';
        }
      },
      error => {
        console.log(error);
        this.deleteStatus = 'error';
        this.message = 'Error al intentar borrar el álbum';
      }
    );
  }
}
