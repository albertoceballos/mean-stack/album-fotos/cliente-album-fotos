import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute,Params } from '@angular/router';
//servicios
import {ImageService} from '../../services/image.service';
//modelo
import {Image} from '../../models/Image';

import {GLOBAL} from '../../services/global';
@Component({
  selector: 'app-edit-image',
  templateUrl: '../add-image/add-image.component.html',
  styleUrls: ['./edit-image.component.css']
})
export class EditImageComponent implements OnInit {

  public pageTitle:string;
  public status:string;
  public message:string;
  public image:Image;
  public url:string;
  public idImage;
  public resultUpload;
  public filesToUpload:Array<File>;
  public is_edit:boolean;
  constructor(private _activatedRoute:ActivatedRoute,private _imageService:ImageService) { 
    this.pageTitle="Editar imagen";
    this.image=new Image('','','','','');
    this.url=GLOBAL.url;
    this.is_edit=true;
  }

  ngOnInit() {
    this._activatedRoute.params.subscribe(
      params=>{
        this.idImage=params['idImage'];
        this.getImage(this.idImage);
      }
    )
  }

  getImage(idImage){
    this._imageService.getImage(idImage).subscribe(
      response=>{
        this.image=response.image;
      },
      error=>{
        console.log(error);
        this.status='error';
        if(error.error.message){
          this.message=error.error.message
        }else{
          this.message='Error en la petición';
        }
      }
    );
  }


  fileChangeEvent(fileInput) {
    this.filesToUpload = fileInput.target.files;
  }

  //subida de imagen
  makeFileRequest(url, params, files) {
    return new Promise((resolve, reject) => {
      var formData = new FormData();
      var xhr = new XMLHttpRequest();
      for (let i = 0; i < files.length; i++) {
        formData.append('image', files[i], files[i].name);
      }
      xhr.onreadystatechange=function(){
        if(xhr.readyState==4){
          if(xhr.status==200){
            resolve(JSON.parse(xhr.response));
          }else{
            reject(xhr.response);
          }
        }
      }
      xhr.open('POST',url,true);
      xhr.send(formData);
    });
  }

  onSubmit(){
    this._imageService.updateImage(this.idImage,this.image).subscribe(
      response=>{
        if(response.image){
          this.image=response.image;
          this.idImage=this.image._id;
          this.makeFileRequest(this.url+'upload-image/'+this.idImage,[],this.filesToUpload)
          .then(
            (result)=>{
              this.resultUpload=result;
              console.log(this.resultUpload);
              this.getImage(this.idImage);
              this.status='success';
              this.message=this.resultUpload.message;
              setTimeout(()=>{
                this.status=null;
              },1500);
            },
            (error)=>{
              console.log(error);
              this.message='Error al subir la imagen';
            }
          );

        }else{
          this.status='error';
          this.message='No se pudo actualizar la imagen';
        }
      },
      error=>{
        console.log(error);
        this.status='error';
        this.message='Error al guardar la imagen';
      }
    );
  }

}
