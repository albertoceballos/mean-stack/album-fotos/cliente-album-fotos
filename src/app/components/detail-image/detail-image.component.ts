import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';

import {Image} from '../../models/Image';

import {ImageService} from '../../services/image.service';

import {GLOBAL} from '../../services/global';

@Component({
  selector: 'app-detail-image',
  templateUrl: './detail-image.component.html',
  styleUrls: ['./detail-image.component.css']
})
export class DetailImageComponent implements OnInit {
  public pageTitle;
  public image:Image;
  public url:string;
  public status:string;
  public message:string;
  constructor(private _activatedRoute:ActivatedRoute, private _imageService:ImageService,private _router:Router) {
    this.url=GLOBAL.url;

   }

  ngOnInit() {
    this._activatedRoute.params.subscribe(
      params=>{
        var idImage=params['idImage'];
        this.getImage(idImage);
      }
    );
  }

  getImage(id){
    this._imageService.getImage(id).subscribe(
      response=>{
        if(response.image){
          this.image=response.image;
          this.pageTitle=this.image.title;
        }else{
          this.status='error';
          this._router.navigate(['/album-list']);
        }
      },
      error=>{
        console.log(error);
        this.status='error';
        if(error.error.message){
          this.message=error.error.message
        }else{
          this.message='Error en la petición';
        }
      }
    );
  }

  deleteImage(id){
    this._imageService.deleteImage(id).subscribe(
      response=>{
        console.log(response);
        if(response.image){
          alert('Foto borrada con éxito');
          this._router.navigate(['/album',response.image.album]);
        }else{
          this.status='error';
          this.message='Error al borrar la foto';
        }
      },
      error=>{
        console.log(error);
        this.status='error';
        this.message='Error al borrar la foto'; 
      }
    );
  }

}
