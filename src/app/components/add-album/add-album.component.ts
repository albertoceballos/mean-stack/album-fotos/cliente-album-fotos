import { Component, OnInit } from '@angular/core';

import { Album } from '../../models/Album';

import { AlbumService } from '../../services/album.service';

@Component({
  selector: 'app-add-album',
  templateUrl: './add-album.component.html',
  styleUrls: ['./add-album.component.css']
})
export class AddAlbumComponent implements OnInit {
  public pageTitle: string;
  public album:Album;
  public status:string;
  public message:string;
  constructor(private _albumService:AlbumService) {
    this.pageTitle = 'Añadir álbum';
    this.album=new Album('','','');
  }

  ngOnInit() {
  }

  onSubmit(){
    this._albumService.addAlbum(this.album).subscribe(
      response=>{
        console.log(response);
        if(response.album){
          this.status='success';
          this.message=response.message;
        }else{
          this.status='error';
          this.message='Error al crear el álbum';
        }
      },
      error=>{
        console.log(error);
        this.status='error';
        if(error.error.message){
          this.message=error.error.message;
        }else{
          this.message='Error al crear el álbum';
        }
      }
    );
  }

}
