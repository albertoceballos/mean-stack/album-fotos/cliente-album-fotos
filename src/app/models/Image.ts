export class Image{
    public _id:number;
    public title:string;
    public picture:string;
    public album:any;
    public created_at:any;

    constructor(id,title,picture,album,created_at){
        this._id=id;
        this.title=title;
        this.picture=picture;
        this.album=album;
        this.created_at=created_at;
    }
}